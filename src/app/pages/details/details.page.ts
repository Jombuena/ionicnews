import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../../api/news.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  details: {
    id: string,
    title: {
      rendered: string;
    },
    content: {
      rendered: string;
    },
    date: Date,
    excerpt: {
      rendered: string;
    },
  };

  imageMedium: any;

  constructor(private restApi: NewsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.imageMedium = this.router.getCurrentNavigation().extras.state.imageMedium;
      }
    });
  }

  getFeed(id: string): Subscription {
    return this.restApi.getArticleDetails(id).subscribe((data: any) => {
      this.details = data;
      if (!this.imageMedium) {
        const mediaUrl = data._links['wp:featuredmedia'][0].href;
        this.restApi.fetchRawData(mediaUrl).subscribe((mediaData: any) => {
          this.imageMedium = mediaData.source_url;
        });
      }
    });
  }

  goBack() {
    this.router.navigate(['home']);
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.getFeed(params.id);
    });
  }

}
