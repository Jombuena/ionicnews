import {Component, OnInit} from '@angular/core';
import {NewsService} from '../../api/news.service';
import {NavigationExtras, Router} from '@angular/router';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    constructor(public restApi: NewsService,
                public router: Router) { }

    feed: [{
        index: number;
        id: string,
        title: {
            rendered: string;
        },
        date: Date;
        imageThumb: string;
        imageMedium: string;
    }];

    getFeed(): void {
        this.restApi.getNewsFeed().subscribe((data: any) => {
            this.feed = data.map((newsData) => {
                const {id} = newsData;
                const imageThumb = newsData._embedded['wp:featuredmedia'][0].media_details.sizes.thumbnail.source_url;
                const imageMedium = newsData._embedded['wp:featuredmedia'][0].media_details.sizes.medium.source_url;
                return ({
                    ...newsData,
                    imageThumb,
                    imageMedium,
                    id,
                });
            });
        });
    }

    onSelect(id: string, i: number): void {
        const navigationExtras: NavigationExtras = {
            state: {
                imageMedium: this.feed[i].imageMedium
            }
        };
        this.router.navigate(['details', id], navigationExtras);
    }

    ngOnInit() {
        this.getFeed();
    }
}
