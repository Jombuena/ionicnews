import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { FeedResult } from '../models/feedResult';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  feedUrl = 'https://www.circuitricardotormo.com/wp-json/wp/v2/posts?page=1&per_page=15&_embed';

  constructor(private http: HttpClient) {}

  getNewsFeed(): Observable<FeedResult> {
    return this.http.get<FeedResult>(`${this.feedUrl}`)
        .pipe(
            retry(1),
            catchError(this.handleError)
        );
  }

  fetchRawData(mediaUrl): Observable<FeedResult> {
    return this.http.get<FeedResult>(mediaUrl)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getArticleDetails(id): Observable<FeedResult> {
    return this.http.get<FeedResult>(`https://www.circuitricardotormo.com/wp-json/wp/v2/posts/${id}?_embedbed`)
        .pipe(
            retry(1),
            catchError(this.handleError)
        );
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
